﻿using NUnit.Framework;
using POST_NCrunch.Domain;

namespace POST_NCrunch.Tests
{
    [TestFixture]
    public class DepartamentoPessoalTests
    {
        [Test]
        public void CalcularSalarioTest()
        {
            DepartamentoPessoal dp = new DepartamentoPessoal();
            
            Funcionario funcionario = new Funcionario();
            funcionario.Nome = "Julio";
            funcionario.Salario = 2000.00m;
            funcionario.PercentualDescontoTransporte = 8m; // R$ 160,00
            funcionario.PercentualBonusPorCargo = 3m;        // R$ 60,00

            decimal salario = dp.CalcularSalario(funcionario);
            
            // Visto que a fórmula de cálculo fictícia é:
            // SALARIO - Desconto c/ Transporte + Bônus por Cargo Ocupado
            // O Retorno deve ser R$ 1.900,00
            Assert.AreEqual(1900m, salario);
        }
    }
}