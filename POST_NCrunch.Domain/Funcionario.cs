﻿namespace POST_NCrunch.Domain
{
    public class Funcionario
    {
        public string Nome { get; set; }
        public decimal Salario { get; set; }
        public decimal PercentualDescontoTransporte { get; set; }
        public decimal PercentualBonusPorCargo { get; set; }
    }
}