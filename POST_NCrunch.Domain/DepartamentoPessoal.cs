﻿namespace POST_NCrunch.Domain
{
    public class DepartamentoPessoal
    {
        public decimal CalcularSalario(Funcionario funcionario)
        {
            decimal valorDescontoTransporte = funcionario.Salario * (funcionario.PercentualDescontoTransporte / 100);
            decimal valorBonusPorCargo = funcionario.Salario * (funcionario.PercentualBonusPorCargo / 100);

            return funcionario.Salario - valorDescontoTransporte + valorBonusPorCargo;
        }
    }
}